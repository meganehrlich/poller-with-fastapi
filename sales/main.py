from db import AutoVOQueries
from fastapi import Depends, FastAPI
from models import AutoVO

app = FastAPI()

@app.get("/api/autos", response_model=list[AutoVO])
def get_autos(queries: AutoVOQueries=Depends()):
    autos = queries.get_all_autos()
    return autos
